package com.example.user.service.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.user.service.dto.ResponseDto;
import com.example.user.service.dto.UserDto;
import com.example.user.service.entity.User;
import com.example.user.service.service.UserService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class UserController {
	
	private final UserService userService;
	
	@PostMapping("/users")
	public ResponseEntity<ResponseDto> addUser(@RequestBody UserDto userDto)
	{
		return new ResponseEntity<ResponseDto>(userService.addUser(userDto), HttpStatus.CREATED);
	}

	
	@GetMapping("/users")
	public ResponseEntity<List<User>> getUsers()
	{
		return new ResponseEntity<>(userService.getUsers(), HttpStatus.CREATED);
	}
	
	@GetMapping("/users/{id}")
	public ResponseEntity<UserDto> getUser(@PathVariable int id)
	{
		return new ResponseEntity<>(userService.getUser(id), HttpStatus.CREATED);
	}
}
