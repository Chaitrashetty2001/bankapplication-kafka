package com.example.account.service.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.account.service.entity.Account;
import com.example.account.service.entity.AccountType;

public interface AccountRepository extends JpaRepository<Account, Integer>{

	Optional<Object> findAccountByUserIdAndAccounttype(int userId, AccountType valueOf);

	Optional<Account> findAccountByAccountNumber(long accountNumber);

}
