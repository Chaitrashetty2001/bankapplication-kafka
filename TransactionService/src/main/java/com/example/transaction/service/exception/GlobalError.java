package com.example.transaction.service.exception;

public class GlobalError {
	
	 public static final String NOT_FOUND = "404";
	 
	 public static final String CONFLICT = "409";

}
