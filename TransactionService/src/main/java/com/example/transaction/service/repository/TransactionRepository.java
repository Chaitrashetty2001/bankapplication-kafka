package com.example.transaction.service.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;

import com.example.transaction.service.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long>{

	List<Transaction> findByAccountNumber(long accountNumber);

}
