package com.example.transaction.service.exception;

public class ResourceConflictException extends GlobalException{
	
	public ResourceConflictException() {
        super("Resource already exists",GlobalError.CONFLICT);
    }


	public ResourceConflictException(String message) {
        super(message,GlobalError.CONFLICT);
    }

}
