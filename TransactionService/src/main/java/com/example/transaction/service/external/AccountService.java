package com.example.transaction.service.external;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.transaction.service.configuration.FeignConfiguration;
import com.example.transaction.service.entity.AccountDto;

@FeignClient(name = "account-service", url = "http://localhost:8082/api/v1", configuration = FeignConfiguration.class)
public interface AccountService {

	@GetMapping("/account")
	public AccountDto getAccount(@RequestParam long accountNumber);
	
	@PutMapping("/account")
	public AccountDto updateAccount(@RequestBody AccountDto accountDto);
	
	@GetMapping("/balance")
	public Double getBalance(@RequestParam long accountNumber);

}
