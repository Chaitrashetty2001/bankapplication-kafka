package com.example.transaction.service.entity;

public record ResponseDto(String message) {
}