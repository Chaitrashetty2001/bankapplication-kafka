package com.example.fundtransfer.service.exception;

public class ResourceNotFound extends GlobalException{

    public ResourceNotFound() {
        super("Resource not found ", GlobalError.NOT_FOUND);
    }

    public ResourceNotFound(String message) {
        super(message, GlobalError.NOT_FOUND);
    }
}

