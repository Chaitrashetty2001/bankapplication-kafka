package com.example.fundtransfer.service.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.fundtransfer.service.dto.FundTransferRequest;
import com.example.fundtransfer.service.dto.ResponseDto;
import com.example.fundtransfer.service.service.FundTransferService;

import lombok.RequiredArgsConstructor;

@RequestMapping("/api/v3")
@RestController
@RequiredArgsConstructor
public class FundTransferController {
	
	private final FundTransferService fundTransferService;
	
	
	 @PostMapping("/")
	    public ResponseEntity<ResponseDto> fundTransfer(@RequestBody FundTransferRequest fundTransferRequest) {
	        return new ResponseEntity<>(fundTransferService.fundTransfer(fundTransferRequest), HttpStatus.CREATED);
	    }


	
	

}
