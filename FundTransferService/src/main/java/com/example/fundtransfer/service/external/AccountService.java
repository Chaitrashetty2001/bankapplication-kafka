package com.example.fundtransfer.service.external;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.fundtransfer.service.configuration.FeignConfiguration;
import com.example.fundtransfer.service.dto.AccountDto;

@FeignClient(name = "account-service", url = "http://localhost:8082/api/v1", configuration = FeignConfiguration.class)
public interface AccountService {

	@GetMapping("/account")
	public AccountDto getAccount(@RequestParam long accountNumber);

}
