package com.example.fundtransfer.service.kafka;

import org.apache.kafka.common.serialization.Serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonSerializer<T> implements Serializer {
	 
		public byte[] serialize(String topic, Object data) {
			byte[] byteValue = null;
			ObjectMapper mapper = new ObjectMapper();
			try {
				byteValue = mapper.writeValueAsBytes(data);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			return byteValue;
		}
	 
}